import React, { Component } from 'react';
import './App.css';

import axios from 'axios'

import "./bootstrap\\css\\bootstrap.min.css"

export default class App extends Component{

  
  state = {
    name : '',
    data : []
  }

  componentDidMount() {
    axios.get(`http://localhost:8080/1`)
      .then(res => {
        const data = res.data;
        this.setState({ data });
      })
      console.log(this.state.data)
  }

  handleChange = event => {
    this.setState({ name : event.target.value })
  }

  handleSubmit = event => {
    event.preventDefault();

    const etudiant = {
      name : this.state.name,
      cin : '900009',
      filiere : 'Telecom'
    };

    axios.post(`http://localhost:8080/2`, { etudiant })
      .then(res => {
        console.log(res);
        console.log(res.data);
      })

  }

  /*async componentDidMount(){
    const data = await axios.get(`http://localhost:8080/1`)
    this.setState({ data });
    console.log(this.state.data)
  }*/

  render(){
    
    return (
      <div>
        <ul>
          { this.state.data.map((data,index) => <li key={index}>{data.all}</li>)}
        </ul>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>
              Person Name:
              <input type="text" name="name" value={this.state.name} onChange={this.handleChange} />
            </label>
            <button type="submit">Add</button>
          </div>
        </form>
      </div>
    );
  }
}

